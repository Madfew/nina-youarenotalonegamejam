﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Manager : MonoBehaviour
{
    public static UI_Manager Instance;

    public GameObject botonesControles;
    public GameObject boton;
    public GameObject botonSalir;
    public GameObject menuDiario;
    public GameObject menuPausa;

    public Animator animPlayer;

    public Button[] botones;

    public Image[] fotos;

    private void Awake()
    {
        Instance = this;
    }

    public void ActivarDiario()
    {
        menuDiario.SetActive(true);
        FindObjectOfType<AudioManager>().Play("OpenBook_FX");
    }

    public void DesactivarDiario()
    {
        menuDiario.SetActive(false);
        FindObjectOfType<AudioManager>().Play("CloseBook_FX");
    }

    public void ActivarPausa()
    {
        menuPausa.SetActive(true);
    }

    public void DesactivarPausa()
    {
        menuPausa.SetActive(false);
    }

    public void ActivarBotones()
    {
        animPlayer.SetBool("close", false);
        botonesControles.SetActive(false);
        FindObjectOfType<AudioManager>().Play("GrabSomething_FX");
    }

    public void DesactivarBotones()
    {
        //Debug.Log("Coger");
        animPlayer.SetBool("close", true);
        botonesControles.SetActive(true);
        FindObjectOfType<AudioManager>().Play("touchbutton_FX");
    }
}
