﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class ActivatePuzzle : MonoBehaviour
{
    //public static ActivatePuzzle Instance;
    public GameObject puzzle;

    private bool active;

    private void Awake()
    {
        //Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        //puzzle.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (active)
        {
            if (CrossPlatformInputManager.GetButtonDown("Action"))
            {
                UI_Manager.Instance.DesactivarBotones();
                puzzle.SetActive(true);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            active = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            active = false;
        }
    }
}
