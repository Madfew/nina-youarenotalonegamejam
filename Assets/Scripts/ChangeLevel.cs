﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ChangeLevel : MonoBehaviour
{
    public GameObject camera1;
    public GameObject camera2;
    public GameObject player;
    public Transform checkPoint;
    public GameObject fade;
    Vector3 position;
    bool activate;
    float timer;
    bool act;

    // Start is called before the first frame update
    void Start()
    {
        position = player.transform.position;
    }
    
    // Update is called once per frame
    void Update()
    {
        if (activate)
        {
            fade.GetComponent<Animator>().SetTrigger("fade");

            GameManager.Instance.checkPoint = checkPoint;
            player.GetComponent<PlayerMovement>().enabled = false;
            position = player.transform.position;
            activate = false;
        }
        if (act)
        {
            StartCoroutine(EsperarYEjecutar(0.5f, MoverJugador));
            timer -= Time.deltaTime;
            if(timer < 0)
            {
                GameManager.Instance.aud += 1;
                GameManager.Instance.items = 0;
                timer = 0;
                act = false;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            
            act = true;
            timer = 1f;
            activate = true;
        }
    }

    public void MoverJugador()
    {
        //FindObjectOfType<AudioManager>().Play(LevelSound);
        camera1.gameObject.SetActive(false);
        camera2.gameObject.SetActive(true);
        player.GetComponent<PlayerMovement>().enabled = true;
        player.transform.position = position + new Vector3(10.5f, 0, 0);
    }

    IEnumerator EsperarYEjecutar(float tiempo, Action accion)
    {
        yield return new WaitForSecondsRealtime(tiempo);
        accion();
    }

}
