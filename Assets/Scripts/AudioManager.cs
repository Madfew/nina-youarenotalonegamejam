using UnityEngine.Audio;
using System;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
	public static AudioManager instance = null;


	public Sound[] sounds;
    float next_spawn_time;
    private void Start()
    {
        next_spawn_time = Time.time + 0;
    }

    void Awake()
	{
		if (instance != null)
		{
			Destroy(gameObject);
		}
		else
		{
			instance = this;
			DontDestroyOnLoad(gameObject);
		}

		foreach (Sound s in sounds)
		{
			s.source = gameObject.AddComponent<AudioSource>();
			s.source.clip = s.clip;
			s.source.loop = s.loop;
            s.source.outputAudioMixerGroup = s.mixerGroup;
		}
	}

    public void Play(string sound)
	{
		Sound s = Array.Find(sounds, item => item.name == sound);
		if (s == null)
		{
			Debug.LogWarning("Sound: " + name + " not found!");
			return;
		}

		s.source.volume = s.volume * (1f + UnityEngine.Random.Range(-s.volumeVariance / 2f, s.volumeVariance / 2f));
		s.source.pitch = s.pitch * (1f + UnityEngine.Random.Range(-s.pitchVariance / 2f, s.pitchVariance / 2f));

        if (Time.time > next_spawn_time)
        {
            //do stuff here (like instantiate)
            s.source.Play();

            //increment next_spawn_time
            next_spawn_time += s.timeNextSpawn;
        }else  if(Time.time == 0)
        {
            s.source.Play();
        }
        
	}

    public void Pause(string sound)
    {
        Sound s = Array.Find(sounds, item => item.name == sound);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
            return;
        }
        if (s.source.isPlaying == true)
        {
            s.source.Stop();
        }
        else
        {
            s.source.UnPause();
        }
    }
    public void UnPause(string sound)
    {
        Sound s = Array.Find(sounds, item => item.name == sound);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
            return;
        }
        if (s.source.isPlaying == false)
        {
            s.source.UnPause();
        }
        
    }
}
