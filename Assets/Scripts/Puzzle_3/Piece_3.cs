﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class Piece_3 : MonoBehaviour
{
    public Vector3 pos;
    private float dist;

    public bool selected;
    public bool placed;
    public bool interactuable;

    public GameObject dragDrop;

    // Start is called before the first frame update
    void Start()
    {
        //transform.position = new Vector2(Random.Range(Camera.main.transform.position.x - 3.5f, Camera.main.transform.position.x + 3.5f), Random.Range(Camera.main.transform.position.y - 0f, Camera.main.transform.position.y + 2f));
        pos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        dist = transform.position.y;
        if (interactuable)
        {
            if (dist < -1.5f)
            {
                if (!selected)
                {
                    if (placed == false)
                    {
                        placed = true;
                        GetComponent<SortingGroup>().sortingOrder = 0;
                        dragDrop.GetComponent<DragDrop_3>().piecePlaced++;
                    }
                }
            }
        }
        else
        {
            if (!selected)
            {
                if (placed == false)
                {
                    GetComponent<SortingGroup>().sortingOrder = 0;
                }
            }
        }
    }
}
