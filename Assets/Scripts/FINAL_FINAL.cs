﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FINAL_FINAL : MonoBehaviour
{
    public PlayerMovement p1;
    public PlayerController p2;
    public GameObject nina;
    public GameObject padre;
    public Transform wp;

    float speed1, speed2;
    bool act;

    // Start is called before the first frame update
    void Start()
    {
        speed1 = 3f;
        speed2 = 2.5f;
    }

    // Update is called once per frame
    void Update()
    {
        if(act == true)
        {
            padre.SetActive(true);
            p1.enabled = false;
            p2.enabled = false;
            nina.transform.position = Vector3.MoveTowards(nina.transform.position, wp.position, speed2 * Time.deltaTime);
            padre.transform.position = Vector3.MoveTowards(padre.transform.position, wp.position, speed1 * Time.deltaTime);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        act = true;
    }

}
