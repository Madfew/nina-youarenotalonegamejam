﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using System;

public class DragDrop_2 : MonoBehaviour
{
    private Touch touch;
    private GameObject selectPiece;

    public GameObject advert;

    public GameObject targetPosition;

    int layer = 1;
    public int piecePlaced = 0;

    private void Awake()
    {
        StartCoroutine(EsperarYEjecutar(0.08f, Chao));
        targetPosition.transform.position = new Vector3(Camera.main.transform.position.x, 3f, 0f);
    }

    void Update()
    {
        if (Input.touchCount > 0)
        {
            touch = Input.GetTouch(0);
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.touches[0].position), Vector2.zero);
                    if (hit.transform.CompareTag("Puzzle"))
                    {
                        if (!hit.transform.GetComponent<Piece_2>().placed)
                        {
                            selectPiece = hit.transform.gameObject;
                            selectPiece.GetComponent<Piece_2>().selected = true;
                            selectPiece.GetComponent<SortingGroup>().sortingOrder = layer;
                            layer++;
                        }
                    }
                    break;
                case TouchPhase.Ended:
                    if (selectPiece != null)
                    {
                        selectPiece.GetComponent<SortingGroup>().sortingOrder = 0;
                        selectPiece.GetComponent<Piece_2>().selected = false;
                        selectPiece = null;
                    }
                    break;
            }
        }

        if (selectPiece != null)
        {
            Vector3 pad = Camera.main.ScreenToWorldPoint(Input.touches[0].position);
            selectPiece.transform.position = new Vector3(pad.x, pad.y, 0);
        }

        if (piecePlaced == 1)
        {
            GameManager.Instance.items += 1;
            UI_Manager.Instance.fotos[0].sprite = Diary.Instance.foto[0];
            UI_Manager.Instance.ActivarBotones();
            UI_Manager.Instance.boton.SetActive(true);
            piecePlaced = 0;
            advert.gameObject.SetActive(false);
            gameObject.SetActive(false);
        }
    }

    IEnumerator EsperarYEjecutar(float tiempo, Action accion)
    {
        yield return new WaitForSecondsRealtime(tiempo);
        accion();
    }

    void Chao()
    {
        UI_Manager.Instance.boton.SetActive(false);
    }
}
