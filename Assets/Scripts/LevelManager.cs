﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance = null;
    public GameObject fade;

    void Start()
    {
        //Instance = this;
        //FindObjectOfType<AudioManager>().Play("Inicio_Music");
    }

    private void Awake()
    {
        Instance = this;
        /*if (Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }*/
    }

    public void WinScene()
    {
        fade.SetActive(true);
        StartCoroutine(EsperarEjecutar(1.5f, CargarWin));
    }

    public void MainMenu()
    {
        fade.SetActive(true);
        StartCoroutine(EsperarEjecutar(1.5f, CargarMenu));
    }

    public void Menu_Creditos()
    {
        fade.SetActive(true);
        StartCoroutine(EsperarEjecutar(1.5f, CargarEscenaCreditos_1));
    }

    public void Win_Creditos()
    {
        fade.SetActive(true);
        StartCoroutine(EsperarEjecutar(1.5f, CargarEscenaCreditos_2));
    }

    public void Gameplay()
    {
        fade.SetActive(true);
        StartCoroutine(EsperarEjecutar(1.5f, CargarEscenaGameplay));
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void CargarMenu()
    {
        FindObjectOfType<SoundFollow>().Musica_IniciarMenu();
        SceneManager.LoadScene(0);
    }

    public void CargarEscenaGameplay()
    {
        FindObjectOfType<SoundFollow>().Musica_IniciarGameplay();
        SceneManager.LoadScene(1);
    }

    public void CargarWin()
    {
        FindObjectOfType<SoundFollow>().Musica_IniciarWin();
        SceneManager.LoadScene(2);
    }

    public void CargarEscenaCreditos_1()
    {
        FindObjectOfType<SoundFollow>().Musica_IniciarCreditos();
        SceneManager.LoadScene(3);
    }

    public void CargarEscenaCreditos_2()
    {
        SceneManager.LoadScene(3);
    }

    IEnumerator EsperarEjecutar(float tiempo, Action accion)
    {
        yield return new WaitForSecondsRealtime(tiempo);
        accion();
    }
}
