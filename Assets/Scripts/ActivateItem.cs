﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class ActivateItem : MonoBehaviour
{
    //public static ActivatePuzzle Instance;
    public int n_foto;
    public GameObject item;
    public bool special_Item;

    private bool active;

    private void Awake()
    {
        //Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        //puzzle.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (active)
        {
            if (CrossPlatformInputManager.GetButtonDown("Action"))
            {
                //actionsound();
                UI_Manager.Instance.DesactivarBotones();
                item.SetActive(true);
            }
        }
    }

    void actionsound()
    {
        FindObjectOfType<AudioManager>().Play("touchbutton_FX");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            active = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            active = false;
        }
    }

    public void Cerrar()
    {
        if (special_Item)
        {
            GameManager.Instance.items += 1;
        }
        UI_Manager.Instance.fotos[n_foto].sprite = Diary.Instance.foto[n_foto];
        item.SetActive(false);
        gameObject.SetActive(false);
    }

}
