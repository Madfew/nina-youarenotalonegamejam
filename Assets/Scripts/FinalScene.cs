﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class FinalScene : MonoBehaviour
{
    public GameObject letraFinal;
    public GameObject fadeNegro;
    public GameObject tapSalir;

    private void Start()
    {
        StartCoroutine(EsperarYEjecutar(6f, Iniciar));
        StartCoroutine(EsperarYEjecutar(7f, LetraFinal));
    }

    IEnumerator EsperarYEjecutar(float tiempo, Action accion)
    {
        yield return new WaitForSecondsRealtime(tiempo);
        accion();
    }

    public void Iniciar()
    {
        fadeNegro.SetActive(true);
    }

    public void LetraFinal()
    {
        tapSalir.SetActive(true);
        letraFinal.SetActive(true);
    }
}
