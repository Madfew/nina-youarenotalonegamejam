﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public bool activePlayer;
    public bool lose;

    public Transform checkPoint;
    public GameObject player;

    public GameObject[] puertaCerrada;
    public GameObject[] puertaAbierta;

    public int items;
    bool act1, act2, act3;
    public int aud;

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        items = 0;
        aud = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (items == 2)
        {
            AbrirPuertas();
        } else if (items == 0)
        {
            CerrarPuertas();
        }

        switch (aud)
        {
            case 3:
                if (!act3)
                {
                    FindObjectOfType<SoundFollow>().Silence3();
                    FindObjectOfType<SoundFollow>().Tercer();
                    act3 = true;
                }
                break;
            case 2:
                if (!act2)
                {
                    FindObjectOfType<SoundFollow>().Silence2();
                    FindObjectOfType<SoundFollow>().Segundo();
                    act2 = true;
                }
                break;
            case 1:
                if (!act1)
                {
                    FindObjectOfType<SoundFollow>().Silence1();
                    FindObjectOfType<SoundFollow>().Primero();
                    act1 = true;
                }
                break;
            default:
                print("Sonando default");
                break;
        }
    }

    public void ResetLevel()
    {
        //FindObjectOfType<SoundFollow>().Silence();
        CameraZoom.Instance.ResetConfig();
        player.transform.position = checkPoint.position;
    }

    public void CerrarPuertas()
    {
        for (int i = 0; i < puertaCerrada.Length; i++)
        {
            puertaCerrada[i].gameObject.SetActive(true);
            puertaAbierta[i].gameObject.SetActive(false);
        }
    }

    public void AbrirPuertas()
    {
        for (int i = 0; i < puertaCerrada.Length; i++)
        {
            puertaCerrada[i].gameObject.SetActive(false);
            puertaAbierta[i].gameObject.SetActive(true);
        }
    }
}
