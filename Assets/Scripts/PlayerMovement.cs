﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerMovement : MonoBehaviour
{
    public PlayerController controller;
    public Animator animPlayer;
    public float runSpeed = 40f;

    float horizontalMove = 0f;

    float timer = 2f;

    void Update()
    {
        if (GameManager.Instance.lose)
        {
            animPlayer.SetBool("Lose", true);
            horizontalMove = 0;
            timer -= Time.deltaTime;
            if (timer < 0)
            {
                animPlayer.SetBool("Lose", false);
                GameManager.Instance.lose = false;
                GameManager.Instance.ResetLevel();
                timer = 0f;
            }
            //gameObject.GetComponent<PlayerMovement>().enabled = false;
        } else
        {
            timer = 2f;
            horizontalMove = CrossPlatformInputManager.GetAxisRaw("Horizontal") * runSpeed;
            animPlayer.SetFloat("mov", Mathf.Abs(horizontalMove));

            if (Mathf.Abs(horizontalMove) >= 0.1)
            {
                GameManager.Instance.activePlayer = true;
                playCaminar();
            }
            else if (Mathf.Abs(horizontalMove) <= 0)
            {
                GameManager.Instance.activePlayer = false;
                pauseCaminar();
            }
        }
    }

    void FixedUpdate()
    {
        // Move our character
        controller.Move(horizontalMove * Time.fixedDeltaTime);
    }

    void playCaminar()
    {
        FindObjectOfType<AudioManager>().Play("Pasos_PJ");
    }

    void pauseCaminar()
    {
        FindObjectOfType<AudioManager>().Pause("Pasos_PJ");
    }
}
