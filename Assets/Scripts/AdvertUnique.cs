﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class AdvertUnique : MonoBehaviour
{
    public TextMeshProUGUI textDisplay;
    public Transform item;
    public float posX;
    public float posY;

    bool act;
    float timer = 1f;

    // Start is called before the first frame update
    void Start()
    {
        Vector3 temp = transform.position;
        temp.x = item.position.x + posX;
        temp.y = item.position.y + posY;
        textDisplay.transform.position = temp;
    }

    private void Update()
    {
        if (act)
        {
            timer -= Time.deltaTime;
            if (timer < 0)
            {
                timer = 0;
                gameObject.SetActive(false);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        textDisplay.DOFade(1f, 0.5f);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        textDisplay.DOFade(0, 0.5f);
        act = true;
    }
}
