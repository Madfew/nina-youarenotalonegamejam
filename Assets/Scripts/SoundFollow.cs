﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundFollow : MonoBehaviour
{
    public static SoundFollow Instance = null;

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    void Start()
    {
        FindObjectOfType<AudioManager>().Play("Inicio_Music");
    }

    public void Musica_IniciarGameplay()
    {
        FindObjectOfType<AudioManager>().Pause("Inicio_Music");

        FindObjectOfType<AudioManager>().Play("Nivel1_Music");
    }

    public void Musica_IniciarMenu()
    {
        FindObjectOfType<AudioManager>().Pause("Nivel1_Music");
        FindObjectOfType<AudioManager>().Pause("Nivel2_Music");
        FindObjectOfType<AudioManager>().Pause("Nivel3_Music");
        FindObjectOfType<AudioManager>().Pause("Nivel4_Music");
        FindObjectOfType<AudioManager>().Pause("End_Music");

        FindObjectOfType<AudioManager>().Play("Inicio_Music");
    }

    public void Musica_IniciarWin()
    {
        FindObjectOfType<AudioManager>().Pause("Nivel4_Music");

        FindObjectOfType<AudioManager>().Play("End_Music");
    }

    public void Musica_IniciarCreditos()
    {
        FindObjectOfType<AudioManager>().Pause("Inicio_Music");

        FindObjectOfType<AudioManager>().Play("End_Music");
    }

    public void Silence1()
    {
        FindObjectOfType<AudioManager>().Pause("Nivel1_Music");
        FindObjectOfType<AudioManager>().Pause("Nivel3_Music");
        FindObjectOfType<AudioManager>().Pause("Nivel4_Music");

        
    }
    public void Silence2()
    {
        FindObjectOfType<AudioManager>().Pause("Nivel1_Music");
        FindObjectOfType<AudioManager>().Pause("Nivel2_Music");
        FindObjectOfType<AudioManager>().Pause("Nivel4_Music");
    }
    public void Silence3()
    {
        FindObjectOfType<AudioManager>().Pause("Nivel1_Music");
        FindObjectOfType<AudioManager>().Pause("Nivel2_Music");
        FindObjectOfType<AudioManager>().Pause("Nivel3_Music");
    }

    public void Silence()
    {
        FindObjectOfType<AudioManager>().Pause("Nivel1_Music");
        FindObjectOfType<AudioManager>().Pause("Nivel2_Music");
        FindObjectOfType<AudioManager>().Pause("Nivel3_Music");
        FindObjectOfType<AudioManager>().Pause("Nivel4_Music");
    }

    public void Primero()
    {
        /*FindObjectOfType<AudioManager>().Pause("Nivel1_Music");
        FindObjectOfType<AudioManager>().Pause("Nivel3_Music");
        FindObjectOfType<AudioManager>().Pause("Nivel4_Music");*/
        FindObjectOfType<AudioManager>().Play("Nivel2_Music");
    }
    public void Segundo()
    {
        /*FindObjectOfType<AudioManager>().Pause("Nivel1_Music");
        FindObjectOfType<AudioManager>().Pause("Nivel2_Music");
        FindObjectOfType<AudioManager>().Pause("Nivel4_Music");*/
        FindObjectOfType<AudioManager>().Play("Nivel3_Music");
    }
    public void Tercer()
    {
       /* FindObjectOfType<AudioManager>().Pause("Nivel1_Music");
        FindObjectOfType<AudioManager>().Pause("Nivel2_Music");
        FindObjectOfType<AudioManager>().Pause("Nivel3_Music");*/
        FindObjectOfType<AudioManager>().Play("Nivel4_Music");
    }

}
