﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Advert : MonoBehaviour
{
    public Image advert;
    public Transform item;
    public float posY;

    // Start is called before the first frame update
    void Start()
    {
        Vector3 temp = transform.position;
        temp.x = item.position.x;
        temp.y = item.position.y + posY;
        advert.transform.position = temp;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        advert.DOFade(0.75f, 0.5f);
        
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        advert.DOFade(0, 0.5f);
    }
}
