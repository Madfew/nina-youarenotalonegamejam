﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Enemy : MonoBehaviour
{
    public float speed;
    private float waitTime;
    public float startTime;

    private bool m_FacingRight = true;

    [SerializeField] Transform[] waypoints;
    int waypointIndex = 0;

    bool attack;
    bool act;
    float move;

    // Start is called before the first frame update
    void Start()
    {
        waitTime = startTime;
        transform.position = waypoints[waypointIndex].transform.position;
        //fase = Fase.Inicia;
    }

    // Update is called once per frame
    void Update()
    {
        if (!attack)
        {
            transform.position = Vector3.MoveTowards(transform.position, waypoints[waypointIndex].position, speed * Time.deltaTime);

            if (Vector2.Distance(transform.position, waypoints[waypointIndex].position) < 0.2f)
            {
                if (waitTime <= 0)
                {
                    if (transform.position == waypoints[waypointIndex].position)
                    {
                        waypointIndex += 1;
                    }
                    if (waypointIndex == waypoints.Length)
                    {
                        waypointIndex = 0;
                    }
                    waitTime = startTime;
                }
                else
                {
                    waitTime -= Time.deltaTime;
                }
            }
        } else if (attack)
        {
            StartCoroutine(EsperarYEjecutar(2f, ResetEnemy));
            CameraZoom.Instance.Zoom();
            GameManager.Instance.lose = true;
        }

        move = transform.position.x - waypoints[waypointIndex].position.x;

        if (move > 0 && !m_FacingRight)
        {
            // ... flip the player.
            Flip();
        }
        // Otherwise if the input is moving the player left and the player is facing right...
        else if (move < 0 && m_FacingRight)
        {
            // ... flip the player.
            Flip();
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (GameManager.Instance.activePlayer && !GameManager.Instance.lose)
            {
                GameManager.Instance.lose = true;
                attack = true;
            }
        }
    }

    IEnumerator EsperarYEjecutar(float tiempo, Action accion)
    {
        yield return new WaitForSecondsRealtime(tiempo);
        accion();
    }

    void ResetEnemy()
    {
        attack = false;
    }

    private void Flip()
    {
        // Switch the way the player is labelled as facing.
        m_FacingRight = !m_FacingRight;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}
