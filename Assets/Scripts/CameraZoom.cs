﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraZoom : MonoBehaviour
{
    public static CameraZoom Instance;

    public float smoothSpeed;
    private CinemachineVirtualCamera vCam;
    private CinemachineFramingTransposer composer;

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        vCam = GetComponent<CinemachineVirtualCamera>();
        composer = vCam.GetCinemachineComponent<CinemachineFramingTransposer>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Zoom()
    {
        vCam.m_Lens.OrthographicSize = Mathf.MoveTowards(vCam.m_Lens.OrthographicSize, 3f, smoothSpeed * Time.deltaTime);
        composer.m_DeadZoneWidth = Mathf.MoveTowards(composer.m_DeadZoneWidth, 0f, smoothSpeed * Time.deltaTime);
    }

    public void ResetConfig()
    {
        vCam.m_Lens.OrthographicSize = 5.23f;
        composer.m_DeadZoneWidth = 0.2f;
    }
}
