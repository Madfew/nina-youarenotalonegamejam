﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class PPmodify : MonoBehaviour
{
    [SerializeField]
    private PostProcessVolume m_PostProcessVolume = null;

    private Bloom _Bloom;
    private Vignette _Vignette;
    private Grain _Grain;
    private LensDistortion _LensDistortion;
    private ChromaticAberration _ChromaticAberration;

    private float lastTime = -1;
    float delta = 0;
    private void Start()
    {

    }
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "PPModifier" && m_PostProcessVolume != null && collision.gameObject.name == "Cortina")
        {
            if (m_PostProcessVolume.profile.TryGetSettings(out _Bloom))
            {
                if (lastTime > 0)
                {
                    delta = Time.time - lastTime;
                }
                lastTime = Time.time;
                _Bloom.intensity.value = Mathf.Lerp(_Bloom.intensity.value, 5, .5f * lastTime);
            }


        }

        if (collision.gameObject.tag == "PPModifier" && m_PostProcessVolume != null && collision.gameObject.name == "Pasadizo")
        {
            //FindObjectOfType<AudioManager>().Pause("Nivel1_Music");
            m_PostProcessVolume.profile.TryGetSettings(out _ChromaticAberration);
            m_PostProcessVolume.profile.TryGetSettings(out _Grain);
            m_PostProcessVolume.profile.TryGetSettings(out _LensDistortion);
            m_PostProcessVolume.profile.TryGetSettings(out _Vignette);
            if (lastTime > 0)
            {
                delta = Time.time - lastTime;
            }
            lastTime = Time.time;
            _Grain.intensity.value = Mathf.Lerp(_Grain.intensity.value, 0.15f, .5f * lastTime);
            _ChromaticAberration.intensity.value = Mathf.Lerp(_ChromaticAberration.intensity.value, 0.1f, .5f * lastTime);
            _LensDistortion.intensity.value = Mathf.Lerp(_LensDistortion.intensity.value, -5, .5f * lastTime);
            _Vignette.intensity.value = Mathf.Lerp(_Vignette.intensity.value, .35f, .5f * lastTime);
        }
        if (collision.gameObject.tag == "PPModifier" && m_PostProcessVolume != null && collision.gameObject.name == "Cocina")
        {
            //FindObjectOfType<AudioManager>().Pause("Nivel2_Music");
            m_PostProcessVolume.profile.TryGetSettings(out _ChromaticAberration);
            m_PostProcessVolume.profile.TryGetSettings(out _Grain);
            m_PostProcessVolume.profile.TryGetSettings(out _LensDistortion);
            m_PostProcessVolume.profile.TryGetSettings(out _Vignette);
            if (lastTime > 0)
            {
                delta = Time.time - lastTime;
            }
            lastTime = Time.time;
            _Grain.intensity.value = Mathf.Lerp(_Grain.intensity.value, 0.2f, .5f * lastTime);
            _ChromaticAberration.intensity.value = Mathf.Lerp(_ChromaticAberration.intensity.value, 0.3f, .5f * lastTime);
            _LensDistortion.intensity.value = Mathf.Lerp(_LensDistortion.intensity.value, -10, .5f * lastTime);
            _Vignette.intensity.value = Mathf.Lerp(_Vignette.intensity.value, .4f, .5f * lastTime);
        }
        if (collision.gameObject.tag == "PPModifier" && m_PostProcessVolume != null && collision.gameObject.name == "Entrada")
        {
            //FindObjectOfType<AudioManager>().Pause("Nivel3_Music");
            m_PostProcessVolume.profile.TryGetSettings(out _ChromaticAberration);
            m_PostProcessVolume.profile.TryGetSettings(out _Grain);
            m_PostProcessVolume.profile.TryGetSettings(out _LensDistortion);
            m_PostProcessVolume.profile.TryGetSettings(out _Vignette);
            if (lastTime > 0)
            {
                delta = Time.time - lastTime;
            }
            lastTime = Time.time;
            _Grain.intensity.value = Mathf.Lerp(_Grain.intensity.value, 0.3f, .5f * lastTime);
            _ChromaticAberration.intensity.value = Mathf.Lerp(_ChromaticAberration.intensity.value, 0.8f, .5f * lastTime);
            _LensDistortion.intensity.value = Mathf.Lerp(_LensDistortion.intensity.value, -20, .5f * lastTime);
            _Vignette.intensity.value = Mathf.Lerp(_Vignette.intensity.value, .5f, .5f * lastTime);
        }
    }



    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PPModifier" && m_PostProcessVolume != null && collision.gameObject.name == "Cortina")
        {
            m_PostProcessVolume.profile.TryGetSettings(out _Bloom);
            if (lastTime > 0)
            {
                delta = Time.time - lastTime;
            }
            lastTime = Time.time;
            _Bloom.intensity.value = Mathf.InverseLerp(_Bloom.intensity.value, 5, .5f * lastTime);



        }
    }
}
