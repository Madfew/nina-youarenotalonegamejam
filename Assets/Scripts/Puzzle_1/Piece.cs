﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class Piece : MonoBehaviour
{
    private Vector3 correctPosition;

    public bool selected;
    public bool placed;

    public GameObject dragDrop;

    // Start is called before the first frame update
    void Start()
    {
        correctPosition = transform.position;
        transform.position = new Vector2(Random.Range(Camera.main.transform.position.x + 4f, Camera.main.transform.position.x + 6f), Random.Range(Camera.main.transform.position.y - 1.5f, Camera.main.transform.position.y + 1.5f));
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(transform.position, correctPosition) < 0.5f)
        {
            if (!selected)
            {
                if (placed == false)
                {
                    transform.position = correctPosition;
                    placed = true;
                    GetComponent<SortingGroup>().sortingOrder = 0;
                    dragDrop.GetComponent<DragDrop>().piecePlaced++;
                }
            }
        }
    }
}
